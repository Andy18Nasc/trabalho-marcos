﻿create database `SIGMA`;
use database `SIGMA`; 



CREATE TABLE `TB_veiculo` (
	`id_veiculo` bigint NOT NULL AUTO_INCREMENT,
	`nmdeplaca` varchar(50) NOT NULL AUTO_INCREMENT,
	`nmpropietario` varchar(50) NOT NULL AUTO_INCREMENT,
	`modelo` varchar(50) NOT NULL AUTO_INCREMENT,
	PRIMARY KEY (`id_veiculo`)
);

CREATE TABLE `TB_cliente` (
	`id_cliente` bigint NOT NULL AUTO_INCREMENT,
	`nome` varchar(100) NOT NULL,
	`cpf` varchar(100) NOT NULL AUTO_INCREMENT,
	`sexo` varchar(100) NOT NULL AUTO_INCREMENT,
	`profissao` varchar(100) NOT NULL AUTO_INCREMENT,
	`datadenascimento` DATE NOT NULL AUTO_INCREMENT,
	`ENDERECO` varchar(100) NOT NULL AUTO_INCREMENT,
	`telefone` int NOT NULL,
	`celular` int NOT NULL AUTO_INCREMENT,
	`email` varchar(100) NOT NULL,
	PRIMARY KEY (`id_cliente`)
);

CREATE TABLE `TB_funcionario` (
	`id_funcionario` bigint NOT NULL AUTO_INCREMENT,
	`nome` varchar NOT NULL,
	`rg` int(50) NOT NULL,
	`cpf` int(50) NOT NULL,
	`sexo` varchar NOT NULL,
	`ENDERECO` varchar NOT NULL,
	`email` varchar(50) NOT NULL,
	PRIMARY KEY (`id_funcionario`)
);

CREATE TABLE `TB_usuario` (
	`id_usuario` varchar NOT NULL AUTO_INCREMENT,
	`nome` varchar NOT NULL,
	`usuario` varchar NOT NULL,
	`senha` varchar NOT NULL,
	PRIMARY KEY (`id_usuario`)
);

CREATE TABLE `TB_enderecocliente` (
	`id_endereco` varchar NOT NULL AUTO_INCREMENT,
	`rua` varchar,
	`bairro` varchar,
	`cidade` varchar,
	`cep` varchar NOT NULL,
	PRIMARY KEY (`id_endereco`)
);

ALTER TABLE `TB_cliente` ADD CONSTRAINT `TB_cliente_fk0` FOREIGN KEY (`ENDERECO`) REFERENCES `TB_enderecocliente`(`id_endereco`);

ALTER TABLE `TB_funcionario` ADD CONSTRAINT `TB_funcionario_fk0` FOREIGN KEY (`ENDERECO`) REFERENCES `TB_enderecocliente`(`id_endereco`);
