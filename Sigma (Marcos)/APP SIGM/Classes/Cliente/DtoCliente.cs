﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APP_SIGM.ClassesBD.Cliente
{
    public class DtoCliente
    {
        public int id { get; set; }
        public string nome { get; set; }
        public int cpf { get; set; }
        public string sexo { get; set; }
        public string profissao { get; set; }
        public string datanascimento { get; set; }
        public string endereco { get; set; }
        public int telefone { get; set; }
        public int celular { get; set; }
        public string email { get; set; }

    }
}
