﻿using APP_SIGM.Classes_Basicas;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APP_SIGM.ClassesBD.Log
{
    class logindatabase
    {

        public bool Logar(string usuario, string senha)

        {
            string script =
                @"select * 
                    from tb_log
                   where nm_usuario = @nm_usuario
                     and ds_senha   = @ds_senha";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_usuario", usuario));
            parms.Add(new MySqlParameter("ds_senha", senha));

            Database db = new Database();
            MySqlDataReader read = db.ExecuteSelectScript(script, parms);
            return Logar();
        }

    }
}

