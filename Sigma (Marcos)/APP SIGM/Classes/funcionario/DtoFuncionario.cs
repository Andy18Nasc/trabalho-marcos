﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APP_SIGM.ClassesBD.funcionario
{
    public class DtoFuncionario
    {
        public int id { get; set; }
        public string nome { get; set; }
        public int cpf { get; set; }
        public string sexo { get; set; }
        public string endereco { get; set; }
        public string email { get; set; }

    }
}
