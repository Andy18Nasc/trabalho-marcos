﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APP_SIGM.ClassesBD.Veiculos
{
    public class Dtoveiculo
    {
        public int id { get; set; }
        public int placa { get; set; }
        public string marca { get; set; }
        public string propietario { get; set; }
        public string modelo { get; set; }
    }
}
